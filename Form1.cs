﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        String curentId;
        public Form1()
        {
            InitializeComponent();
            getAllListAsync();
            //getListByID(10008);
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            

        }

        public async Task getAllListAsync()
        {
            string URI = "http://localhost:50371/api/GetEmployees";
            List<Employee> employees = new List<Employee>();
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync(URI))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var fileJsonString = await response.Content.ReadAsStringAsync();

                        dataGridView1.DataSource = JsonConvert.DeserializeObject<Employee[]>(fileJsonString).ToList();
                    }
                }
            }
        }

        public async Task getListByID(int id)
        {
            string URI = "http://localhost:50371/api/GetEmployee"+"?id="+id;
            List<Employee> employees = new List<Employee>();
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync(URI))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var fileJsonString = await response.Content.ReadAsStringAsync();
                        dataGridView1.DataSource = JsonConvert.DeserializeObject<Employee[]>(fileJsonString).ToList();
                    }
                }
            }
        }

        public async Task deleteEmployee(int id)
        {
            string URI = "http://localhost:50371/api/DeleteEmployee" + "?id=" + id;
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {
                    MessageBox.Show(response.StatusCode + "");
                    if (response.IsSuccessStatusCode)
                    {
                        await getAllListAsync();
                    }
                }
            }
        }

        public async Task addEmployee()
        {
            string URI = "http://localhost:50371/api/PostEmployee";
            Employee employee = new Employee();
           
            employee.name = tName.Text;
            employee.designation = tDesignation.Text;
            employee.location = tLocation.Text;
          
            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(employee), Encoding.UTF8, "application/json");
                using (var response = await client.PostAsync(URI,content))
                {
                    MessageBox.Show(response.StatusCode + "");
                  
                    if (response.IsSuccessStatusCode)
                    {
                        await getAllListAsync();
                    }
                }
            }
        }

        public async Task updateDataAsync()
        {
            string URI = "http://localhost:50371/api/PutEmployee" + "?id=" + curentId;
            List<Employee> employees = new List<Employee>();
            Employee employee = new Employee();
            employee.id = curentId;
            employee.name = tName.Text;
            employee.designation = tDesignation.Text;
            employee.location = tLocation.Text;

            using (var client = new HttpClient())
            {
                using (var response = await client.PutAsJsonAsync(URI,employee))
                {
                   
                    if (response.IsSuccessStatusCode)
                    {
                        await getAllListAsync();
                    }
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    curentId = row.Cells[0].Value.ToString();
                    string name = row.Cells[1].Value.ToString();
                    string designation = row.Cells[2].Value.ToString();
                    string location = row.Cells[3].Value.ToString();
                    tID.Text =curentId;
                    tName.Text = name;
                    tDesignation.Text = designation;
                    tLocation.Text = location;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bUpdate_Click(object sender, EventArgs e)
        {
            updateDataAsync();
        }

        private void bDelete_Click(object sender, EventArgs e)
        {
            deleteEmployee(int.Parse(curentId));
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            addEmployee();
        }
    }
}
