using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp1
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        private static async Task getDataAsync()
        {
            //// In the class
            //HttpClient client = new HttpClient();

            //// Put the following code where you want to initialize the class
            //// It can be the static constructor or a one-time initializer
            //client.BaseAddress = new Uri("http://localhost:50371/api/");
            //client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.Accept.Add(
            //    new MediaTypeWithQualityHeaderValue("application/json"));

           
            //var response = await client.GetAsync("products");
            //var data = await response.Content.ReadAsAsync<IEnumerable<Product>>();
            //this.productBindingSource.DataSource = data;
        }
    }
}
