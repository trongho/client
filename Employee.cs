﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WinFormsApp1
{
    class Employee
    {
        public String id { set; get; }
        public String name { set; get; }
        public String designation { set; get; }
        public String location { set; get; }

        public Employee(string id, string name, string designation, string location)
        {
            this.id = id;
            this.name = name;
            this.designation = designation;
            this.location = location;
        }

        public Employee()
        {
        }
    }
}
